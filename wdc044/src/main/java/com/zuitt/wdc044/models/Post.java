package com.zuitt.wdc044.models;

import javax.persistence.*;

// mark this Java object as a representation of a database table via @Entity
@Entity
// designate table name via @Table
@Table(name="posts")
public class Post {
    // indicates that this property represents the primary key via @Id
    @Id

    // values for this property will be auto-increments - has only ne system in numbering objects
    @GeneratedValue
    private  Long id;

    // class properties that represent table columns in a ration database are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;

    // default constructor - this is needed when retrieving posts
    public Post() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }
}
