package com.zuitt.wdc044.models;

import javax.persistence.*;

@Entity

@Table(name="users")
public class User {
    @Id

    // values for this property will be auto-increments - has only ne system in numbering objects
    @GeneratedValue
    private  Long id;

    // class properties that represent table columns in a ration database are annotated as @Column
    @Column
    private String username;

    @Column
    private String password;

    // default constructor - this is needed when retrieving posts
    public User() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
